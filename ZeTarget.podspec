Pod::Spec.new do |s|


  s.name         = "ZeTarget"
  s.version      = "0.0.2"
  s.summary      = "ZeTarget SDK is for Inter Apps"

  s.description  = <<-DESC
                  ZeTarget SDK is a very helpful tool to report bugs, discuss with developers.
                  It will act as an interface between developer , designer , and tester.
                   DESC

  s.homepage     = "http://www.zetarget.com"
  s.screenshots  = "http://zetarget.io/assets/website/img/zetarget_full_logo.png"

  s.license      = 'Commercial'

  s.author             = { "Raja Vikram" => "rajavikram@zemosolabs.com" }
  # s.social_media_url   = "http://twitter.com/Raja Vikram"

  s.platform     = :ios, "7.0"

  s.source       = { :git => "https://bitbucket.org/rajavikram/zetarget.git" , :tag => "v#{s.version}" }

  s.source_files  = "Debug-iphoneos/include/ZeTarget/ZeTarget.h" , "Debug-iphoneos/include/ZeTarget/HorizontalInAppViewController.xib" , "Debug-iphoneos/include/ZeTarget/InfoInAppViewController.xib" , "Debug-iphoneos/include/ZeTarget/RatingInAppViewController.xib" , "Debug-iphoneos/include/ZeTarget/VerticalInAppViewController.xib"

  s.public_header_files = "Debug-iphoneos/include/ZeTarget/ZeTarget.h"

  s.preserve_paths = "Debug-iphoneos/libZeTarget.a" , "Debug-iphoneos/include/ZeTarget/HorizontalInAppViewController.xib" , "Debug-iphoneos/include/ZeTarget/InfoInAppViewController.xib" , "Debug-iphoneos/include/ZeTarget/RatingInAppViewController.xib" , "Debug-iphoneos/include/ZeTarget/VerticalInAppViewController.xib"

  s.vendored_libraries = "Debug-iphoneos/libZeTarget.a"

  s.library = "sqlite3"

  s.requires_arc = true

  s.dependency 'AFNetworking' , '~> 2.5'
  s.dependency 'Lookback'

end